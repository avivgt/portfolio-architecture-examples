= Self-Healing Infrastructure
Camry Fedei @cfedei
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify
:toc: left

== Solution summary
Modern application infrastructure has become more complex as it has become more powerful and easy to
consume. Keeping infrastructure safe and compliant is a challenge for many organizations. One of the most powerful
approaches to infrastructure management today is the combination of using historical data-driven insights and
automation tools for applying remediation across a scaling estate of hosts in a targeted and prioritized manner.

*Use case:* Managing security, policy and patches for a large number of servers in data centers or public/private clouds.

--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/intro-marketectures/self-healing-marketing-slide.png[750,700]
--

== Summary video
video::JOT8If4F27k[youtube]


image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/logical-diagrams/self-healing-ld.png[750, 700]


== The technology

https://www.redhat.com/en/technologies/management/smart-management[*Red Hat Smart Management*] securely manages any environment supported by Red Hat Enterprise Linux.

https://www.redhat.com/en/technologies/management/ansible[*Red Hat Ansible Automation Platform*] is used for adding a powerful layer of automation to a customer's environment, and can be leveraged as
another means of distributing remediation throughout an estate.

https://www.redhat.com/en/technologies/management/insights[*Red Hat Insights*] services each perform their individual functions, and the customer can choose which services fit their specific needs. For example, Compliance will assess a systems status against a set of compliance rules, and Vulnerability assesses any security risks that may be currently active in the environment.

https://www.redhat.com/en/technologies/cloud-computing/openshift/try-it[*Red Hat OpenShift*] is an enterprise-ready Kubernetes container platform built for an open hybrid cloud strategy. Here, it provides a consistent application platform to manage hybrid cloud, multicloud, and edge deployments.

== Self-Healing Infrastructure (network)
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/self-healing-sd-net.png[750, 700]
--

In this network configuration, you can see the internal network depicted in light blue, where all that is required on the customer estate is the hosted client systems, Red Hat Smart Management, and Red Hat Ansible Automation Platform. These are able to simply communicate amongst themselves on an internal network. From here, Ansible and Smart Management connect on a secure network out to Red Hat, where the data is then processed by Red Hat Insights, and displayed on the Hybrid Cloud Console, of which they communicate to each other internally within Red Hat's network. Once processed, that data is passed back to Ansible and Smart Management on the same channel as originally. 


== Self-Healing Infrastructure (data)
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/self-healing-sd-data.png[750, 700]
--

The data path, starting from the hosts, delivers data to Red Hat Satellite. Here, both Ansible's automation controller and Satellite
transmit the anonymized data to Red Hat where the cloud services internally analyze what issues might be present,
what solutions are available, and what remediation plan we have to move forward with.

From there, these remediation plans, and any associated playbooks, are passed back to the customer's environment, where
Satellite will orchestrate the application of which issues have automated solutions, as well as what package updates
will be delivered to any applicable Red Hat Enterprise Linux (RHEL) system(s), and the automation controller will apply any
remediations to Ansible Automation Platform.

Explicitly stepping through the process:  
 
1. Client hosts register to Satellite which initially collects RHEL system data for Red Hat Insights.  
2. Smart Management and Ansible Automation Platform secure connections to the Hybrid Cloud Console.  
3. Anonymized data is analyzed by Red Hat:  
Insights services analyze data against known issues as well as customer defined parameters.  
Insights for RHEL generate remediation plans and configures playbooks to return to the Smart Management platform.  
Insights for Ansible generates remediation to return to the automation controller.  
4. Remediation assets download to Satellite from Red Hat hosted Insights, on-demand.  
5. RHEL remediation is delivered to the client systems.  
Ansible runner automates running the remediation playbooks on multiple systems at once, and anything requiring manual
configuration is outlined in the remediation plan in the Satellite.  
Any package updates are pulled from Red Hat Satellite. Remediation status is outlined in the dashboard in the Satellite.  
6. Ansible Syncs with Red Hat  
Ansible remediation is delivered to Ansible Automation Platform  
RHEL remediation plans can also be manually synced to the automation controller (optional).  
7. Ansible’s automation controller can be used to deliver the additional remediation synced (optional) in tandem with
it’s normal automation workflow.  

== Download diagrams
View and download all of the diagrams above in our open source tooling site.
--
https://www.redhat.com/architect/portfolio/tool/index.html?#gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/diagrams/self-healing.drawio[[Open Diagrams]]
--

== Provide feedback 
You can offer to help correct or enhance this architecture by filing an https://gitlab.com/osspa/portfolio-architecture-examples/-/blob/main/self-healing.adoc[issue or submitting a merge request against this Portfolio Architecture product in our GitLab repositories].
