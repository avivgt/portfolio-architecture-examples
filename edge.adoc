= Portfolio Architecture Edge Collection
 Ishu Verma  @ishuverma
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify


This is a collection of various architectures for edge computing. Customers are increasing looking to edge
computing to improve operational efficiencies, enhance end-user experiences, meet data residency requirements, and run
resilient, semi-autonomous applications.

[cols="3,7"]
|===
|Title | Description

|link:cloudlets-at-edge.adoc[Cloudlets at edge]
|Providing a consistent infrastructure experience from cloud to edge and enabling modern containerized applications at edge.

|link:datacenter-to-edge.adoc[Data center to edge]
|Bringing computing closer to the edge by monitoring for potential issues with gas pipeline (edge).

|link:edge-medical-diagnosis.adoc[Edge medical diagnosis]
|Accelerating medical diagnosis using condition detection in medical imagery with AI/ML at medical facilities.

|link:industrial-edge.adoc[Industrial edge]
|Boosting manufacturing efficiency and product quality with artificial intelligence and machine learning out to the edge.

|link:scada-interface.adoc[SCADA interface modernisation]
|Provide interfaces with SCADA systems that are compliant with NERC regulations, creating different layers of API
gateways to protect business service depending on the network zones.
|===
